#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

const char const *own_protocol = "http";
const char const *own_hostname = "demohost";
/* best keep them sorted (see the unrelated but similar key_state_update for precise requirements) */
const char const *own_paths[] = {
	"spot1/r",
	"spot1/g",
	"spot1/b",
	"spot2/r",
	"spot2/g",
	"spot2/b",
	"ambient/r",
	"ambient/g",
	"ambient/b",
	"ambient/w",
	NULL,
};

#define URL_MAX_LEVELS_BELOW UINT8_MAX
#define URL_PATH_MAXLENGTH UINT8_MAX
#define URL_PATH_MAXCOUNT UINT8_MAX
/** A struct url_s represents a full absolute URI as precisely as we need to
 * know to decide whether it's one of our own_paths, and sufficient data to
 * join it with a relative URI and then still have such a struct url_s. */
struct url_s {
	bool is_own_protocol;
	bool is_own_host;
	uint8_t closest_path;
	uint8_t closest_path_length;
	uint8_t levels_below; /* starts counting at 1. 0 means that the path is only closest_path[:closest_path_length]. otherwise, the path is always trimmed, and a value of 1 means any (unknown) file name in that path, and 2 means that there is an additional slash. */
};

static void initialize_own_url(struct url_s *u)
{
	u->is_own_protocol = true;
	u->is_own_host = true;
	u->closest_path = 0;
	u->closest_path_length = 0;
	u->levels_below = 0; /* this works both for a base URI of / and a base URI of "/something" where something contains no slashes and does not share a prefix with any of the own_paths */
}

/* The own_path value that is precisely matched by u, or NULL */
static const char *get_own_path(struct url_s *u)
{
	if (!u->is_own_protocol) return NULL;
	if (!u->is_own_host) return NULL;
	if (u->levels_below) return NULL;
	if (own_paths[u->closest_path][u->closest_path_length] == 0) return own_paths[u->closest_path];
	else return NULL;
}

static void print_url(struct url_s *u)
{
	if (!u->is_own_protocol) { printf("URL of foreign protocol\n"); return; }
	if (!u->is_own_host) { printf("'%s' URL of foreign host\n", own_protocol); return; }
	if (u->levels_below) { printf("URL %d levels below first %d path bytes of '%s://%s/%s'\n", u->levels_below, u->closest_path_length, own_protocol, own_hostname, own_paths[u->closest_path]); return; }
	if (own_paths[u->closest_path][u->closest_path_length] == 0) {
		printf("Exactly '%s://%s/%s'\n", own_protocol, own_hostname, own_paths[u->closest_path]);
	} else {
		printf("Shortened '%s://%s/%s' to %d bytes of path\n", own_protocol, own_hostname, own_paths[u->closest_path], u->closest_path_length);
	}
}

/** Reset the URL back to the latest slash, as if `./` has been appended to it */
static void url_trim(struct url_s *url)
{
	if (url->levels_below > 1) return; /* no distinction there */
	if (url->levels_below == 1) {
		url->levels_below = 0;
		return;
	}

	/* rewind back to the last slash */
	while (url->closest_path_length && own_paths[url->closest_path][url->closest_path_length - 1] != '/')
		url->closest_path_length --;

	/* rewind up in the list */
	while (url->closest_path != 0 && memcmp(&own_paths[url->closest_path], &own_paths[url->closest_path - 1], url->closest_path_length) == 0) url->closest_path --;
}

/** Go up one level on a URL, as if `../` has been appended to it */
static void url_up(struct url_s *url)
{
	if (url->levels_below > 2) {
		url->levels_below --;
		return;
	} else if (url->levels_below == 2) {
		url->levels_below = 0;
		return;
	} else if (url->levels_below == 1) {
		url->levels_below = 0;
		/* and continue, because this was just the initial trimming */
	}

	url_trim(url);
	if (url->closest_path_length) {
		url->closest_path_length --;
		url_trim(url);
	}
}

/** Reset the path component as if a `/` were appended */
static void url_all_up(struct url_s *url)
{
	url->closest_path = 0;
	url->closest_path_length = 0;
	url->levels_below = 0;
}

/** This is strictly an append operation; all semantic `./`, `../` and `/` need to be filtered out before. */
static void url_append(struct url_s *url, char byte)
{
	if (url->levels_below) {
		if (byte == '/' && url->levels_below < URL_MAX_LEVELS_BELOW) url->levels_below ++;
		return;
	}
	uint8_t initial_closest_path = url->closest_path;
	while(own_paths[url->closest_path] != NULL)
	{
		if (own_paths[url->closest_path][url->closest_path_length] == byte && byte != 0 && url->closest_path_length != URL_PATH_MAXLENGTH) {
			/* successfully matched another byte */
			url->closest_path_length ++;
			return;
		}
		if (own_paths[url->closest_path+1] == NULL)
			break;
		if (memcmp(own_paths[url->closest_path], own_paths[url->closest_path + 1], url->closest_path_length) == 0) {
			if (url->closest_path == URL_PATH_MAXCOUNT) break;
			url->closest_path ++;
		} else break;
	}
	url->closest_path = initial_closest_path; /* trimming won't roll up on a trailing slash */
	url_trim(url);
	url->levels_below ++;
}

/** Additional information kept while parsing a relative URI atop a struct
 * url_s. The underlying URI should not be considered valid while parsing is in
 * progress, and is modified in-place. */
struct url_state_s {
	struct url_s *url;
	/* names always describe what is expected to follow, unless it's AFTER_*.
	 *
	 * USP_MAYBE_PROTOCOLO_BUT_NOT_MINE is largely treated like
	 * USP_RELATIVE -- incoming data will be resolved relatively, and if a
	 * colon occurs before a slash does, the url is reset to "not my
	 * protocol" anyway.
	 *
	 * USP_SECONDSLASH is set both after '/' and after 'http:/'; while this
	 * allows URLs like http:/my/page to be interpreted as
	 * http://not my hostname/my/page, I don't think this is particularly bad
	 *
	 * USP_RELATIVE_AFTER_SLASH describes all situations in which ./ or ../
	 * have meanings.
	 * */
	enum {USP_MAYBE_PROTOCOL, USP_MAYBE_PROTOCOL_BUT_NOT_MINE, USP_FIRSTSLASH, USP_SECONDSLASH, USP_NETLOC, USP_RELATIVE, USP_RELATIVE_AFTER_SLASH, USP_AFTER_SLASHDOT, USP_AFTER_SLASHDOTDOT, USP_IGNORETHEREST} phase;
	/* those two could probably be unioned */
	uint8_t bytes_maybe_into_own_protocol;
	uint8_t bytes_into_own_host_name;
};

static void initialize_url_state(struct url_state_s *s, struct url_s *initial_url)
{
	s->url = initial_url;
	s->bytes_maybe_into_own_protocol = 0;
	s->bytes_into_own_host_name = 0;
	s->phase = USP_MAYBE_PROTOCOL;
}

static void update_url_state(struct url_state_s *s, char byte)
{
	if (s->phase == USP_MAYBE_PROTOCOL) {
		if ((byte == '/' && s->bytes_maybe_into_own_protocol) || byte == 0) {
			s->phase = USP_RELATIVE_AFTER_SLASH;
			for (int i = 0; i < s->bytes_maybe_into_own_protocol; ++i) update_url_state(s, own_protocol[i]);
		} else if (byte == '/') {
			if (s->url->is_own_protocol)
				s->phase = USP_SECONDSLASH;
			else
				s->phase = USP_IGNORETHEREST;
			return;
		} else if (byte == ':') {
			if (own_protocol[s->bytes_maybe_into_own_protocol] == 0) {
				s->url->is_own_protocol = true;
				s->phase = USP_FIRSTSLASH;
			} else {
				s->url->is_own_protocol = false;
				s->phase = USP_IGNORETHEREST;
			}
			s->url->is_own_host = false;
			url_all_up(s->url);
			return;
		} else if (byte == own_protocol[s->bytes_maybe_into_own_protocol]) {
			/* byte is already checked not to be 0 */
			s->bytes_maybe_into_own_protocol ++;
			return;
		} else {
			s->phase = USP_MAYBE_PROTOCOL_BUT_NOT_MINE;
			url_trim(s->url);
			for (int i = 0; i < s->bytes_maybe_into_own_protocol; ++i) update_url_state(s, own_protocol[i]);
		}
	}
	if (s->phase == USP_FIRSTSLASH) {
		if (byte == '/') {
			s->phase = USP_SECONDSLASH;
		} else {
			s->phase = USP_IGNORETHEREST;
			s->url->is_own_host = false;
		}
		return;
	}
	if (s->phase == USP_SECONDSLASH) {
		if (byte == '/') {
			s->phase = USP_NETLOC;
			return;
		} else {
			s->phase = USP_RELATIVE_AFTER_SLASH;
			url_all_up(s->url);
			return;
		}
	}
	if (s->phase == USP_NETLOC) {
		if (byte == '/' || byte == 0) {
			if (own_hostname[s->bytes_into_own_host_name] == 0) {
				s->phase = USP_RELATIVE_AFTER_SLASH;
				s->url->is_own_host = true;
			} else {
				s->phase = USP_IGNORETHEREST;
				s->url->is_own_host = false;
			}
			url_all_up(s->url);
			return;
		} else {
			if (byte == own_hostname[s->bytes_into_own_host_name]) {
				s->bytes_into_own_host_name ++;
			} else {
				s->url->is_own_host = false;
				url_all_up(s->url);
				s->phase = USP_IGNORETHEREST;
			}
			return;
		}
	}
	if (s->phase == USP_MAYBE_PROTOCOL_BUT_NOT_MINE) {
		if (byte == ':') {
			s->url->is_own_protocol = false;
			s->url->is_own_host = false;
			url_all_up(s->url);

			s->phase = USP_IGNORETHEREST;
			return;
		} else if (byte == '/') {
			s->phase = USP_RELATIVE_AFTER_SLASH;
		}
	}

	if (s->phase == USP_RELATIVE_AFTER_SLASH) {
		if (byte == '.') {
			s->phase = USP_AFTER_SLASHDOT;
			return;
		} else if (byte == '/') {
			return;
		}

		s->phase = USP_RELATIVE;
	}
	if (s->phase == USP_AFTER_SLASHDOT) {
		if (byte == '.') {
			s->phase = USP_AFTER_SLASHDOTDOT;
			return;
		} else if (byte == '/') {
			s->phase = USP_RELATIVE_AFTER_SLASH;
			return;
		} else {
			s->phase = USP_RELATIVE;
			url_append(s->url, '.');
		}
	}
	if (s->phase == USP_AFTER_SLASHDOTDOT) {
		if (byte == '/') {
			url_up(s->url);
			s->phase = USP_RELATIVE;
			return;
		} else {
			s->phase = USP_RELATIVE;
			url_append(s->url, '.');
			url_append(s->url, '.');
		}
	}
	if (s->phase == USP_RELATIVE || s->phase == USP_MAYBE_PROTOCOL_BUT_NOT_MINE) {
		if (byte == '.') {
			s->phase = USP_AFTER_SLASHDOT;
			return;
		}

		if (byte == '?' || byte == '#') {
			s->phase = USP_IGNORETHEREST;
			return;
		}

		if (byte != 0)
			url_append(s->url, byte);
	}
}

static void finalize_url_state(struct url_state_s *s)
{
	update_url_state(s, 0);
}

/* as long as those limits are met, we expect to still be able to *10 + digit, and flip the sign at the end */
#define VALUE_STATE_MAX_SAFE (UINT32_MAX / 10 - 10)
#define VALUE_STATE_EXPONENT_MIN INT8_MIN
#define VALUE_STATE_EXPONENT_MAX INT8_MAX
#define VALUE_STATE_FLOAT_EXPONENT_MAX UINT8_MAX
#define VALUE_OVERFLOW_SENTINEL INT32_MAX
#define VALUE_NOTPRESENT_SENTINEL INT32_MIN
struct value_state_s {
	uint32_t value;
	/* practical exponent is in_number_exponent + float_exponent; number_exponent is updated while reading the mantissa, float_exponent is read after the 'e' */
	int8_t in_number_exponent;
	uint8_t float_exponent;
	bool negative;
	bool exponent_negative;
	bool dot_found;
	bool in_exponent;
	bool overflow;
	bool anydata;
};

static void initialize_value_state(struct value_state_s *s)
{
	s->value = 0;
	s->in_number_exponent = 0;
	s->float_exponent = 0;
	s->negative = false;
	s->exponent_negative = false;
	s->dot_found = false;
	s->in_exponent = false;
	s->overflow = false;
	s->anydata = false;
}

static void update_value_state(struct value_state_s *s, char byte)
{
	if (s->overflow) return;
	s->anydata = true;

	if ('e' == byte || 'E' == byte) {
		s->in_exponent = true;
		return;
	}
	if ('.' == byte) {
		s->dot_found = true;
		return;
	}
	if ('-' == byte) {
		if (s->in_exponent) s->exponent_negative = true;
		else s->negative = true;
		return;
	}
	if ('0' <= byte && byte <= '9') {
		uint8_t digit = byte - '0';

		if (s->in_exponent) {
			int_least16_t new_exponent = s->float_exponent * 10 + digit;
			if (new_exponent > VALUE_STATE_FLOAT_EXPONENT_MAX)
				s->overflow = true;
			else
				s->float_exponent = new_exponent;
		} else {
			if (s->value > VALUE_STATE_MAX_SAFE) {
				/* fiddle with in_number_exponent? */
				if (s->dot_found) {
					/* just ignore trailing digits */
				} else {
					if (s->in_number_exponent < VALUE_STATE_EXPONENT_MAX) s->in_number_exponent ++;
					else s->overflow = true;
				}
			} else {
				if (s->dot_found) {
					if (s->in_number_exponent > VALUE_STATE_EXPONENT_MIN) s->in_number_exponent --;
					else return; /* ignore trailing digits even thouh there'd be room left in the mantissa, but we can't have them in the exponent any more */
				}
				s->value = s->value * 10 + digit;
			}
		}
	}
}

static int32_t finalize_value_state(struct value_state_s *s)
{
	if (s->overflow) return VALUE_OVERFLOW_SENTINEL;
	if (!s->anydata) return VALUE_NOTPRESENT_SENTINEL;

	int_least16_t resulting_exponent = s->in_number_exponent + (s->exponent_negative ? -1 : 1) * s->float_exponent;
	while (resulting_exponent < 0) {
		s->value /= 10;
		resulting_exponent ++;
	}
	while (resulting_exponent > 0) {
		if (s->value < VALUE_STATE_MAX_SAFE) {
			s->value *= 10;
			resulting_exponent --;
		} else return VALUE_OVERFLOW_SENTINEL;
	}
	if (s->value > INT32_MAX) return VALUE_OVERFLOW_SENTINEL;
	if (s->negative) return -s->value;
	else return s->value;
}

const char const *base_keys[] = {"bn", NULL};
const char const *data_keys[] = {"n", "v", NULL};

#define KEY_MAXCHOICE UINT8_MAX
#define KEY_MAXLENGTH UINT8_MAX

struct key_state {
	const char const **keys;
	uint8_t choice;
	uint8_t length;
};

static void key_state_init(struct key_state *key_state, const char const *keys[])
{
	key_state->keys = keys;
	key_state->choice = 0;
	key_state->length = 0;
}

/* keys must be sorted (or at least grouped by any shared starts, and if a
 * string is as short as a shared start, it must come first) */
static void key_state_update(char byte, struct key_state *key_state)
{
	while(key_state->keys[key_state->choice] != NULL)
	{
		if (key_state->keys[key_state->choice][key_state->length] == byte && byte != 0) {
			/* successfully matched another byte */
			if (key_state->length == KEY_MAXLENGTH) goto fail;
			key_state->length ++;
			return;
		}
		if (key_state->keys[key_state->choice+1] == NULL)
			goto fail; /* out of options */
		if (memcmp(key_state->keys[key_state->choice], key_state->keys[key_state->choice + 1], key_state->length) == 0) {
			if (key_state->choice == KEY_MAXCHOICE) goto fail;
			key_state->choice ++;
		} else goto fail;
	}
fail:
	while(key_state->keys[++key_state->choice] != NULL);
}

static const char *key_state_collect(struct key_state *key_state)
{
	/* options already exhausted */
	if (key_state->keys[key_state->choice] == NULL)
		return NULL;
	/* input too short */
	if (key_state->keys[key_state->choice][key_state->length] != 0)
		return NULL;
	return key_state->keys[key_state->choice];
}


#define MAXDEPTH UINT8_MAX

struct parser_state {
	uint8_t object_depth;
	uint8_t array_depth;

	/* this coule be packed more densely, as the error states are exclusive
	 * with everything else, and barely anything here needs more than a few
	 * bits anyway */
	enum {E_NONE, E_OVERFLOW, E_NOTJSON, E_TYPEERROR} error;
	enum {UD_KEY, UD_DISCARD, UD_VALUE, UD_URL} update_destination;
	bool in_data_part; /* true for every second top-level array member */
	enum {SS_OUTSIDE, SS_INSIDE, SS_ESCAPE} string_state;

	struct url_s bn; /* ca. 32 bit with reasonable packing */
	struct url_s n;
	int32_t v;
	union { /* all of those fit in 64bit with some packing (assuming 32bit pointers) :*/
		struct key_state key_state;
		struct url_state_s url_state;
		struct value_state_s value_state;
	};
};

static void string_ended(struct parser_state *ps)
{
	if (ps->update_destination == UD_KEY) {
		const char *result = key_state_collect(&ps->key_state);
		if (ps->in_data_part) {
			if (result == data_keys[0]) {
				ps->update_destination = UD_URL;
				initialize_url_state(&ps->url_state, &ps->n);
			} else if (result == data_keys[1]) {
				ps->update_destination = UD_VALUE;
				initialize_value_state(&ps->value_state);
			} else {
				ps->update_destination = UD_DISCARD;
			}
		} else {
			if (result == base_keys[0]) {
				ps->update_destination = UD_URL;
				/* in order for base dictionaries to behave like patches, we're always interpreting it relative to the document uri */
				initialize_own_url(&ps->bn);
				initialize_url_state(&ps->url_state, &ps->bn);
			} else {
				ps->update_destination = UD_DISCARD;
			}
		}
	} else if (ps->update_destination == UD_URL) {
		finalize_url_state(&ps->url_state);
		ps->update_destination = UD_KEY;
	}
}

static void maybe_start_new_key(struct parser_state *ps)
{
	/* not checking for possible values of ps->update_destination:
	 * if someone chose to have plain comma separated values in
	 * their json objects, we'll just ignore them */
	if (ps->object_depth == 1 && ps->array_depth == 1 && ! ps->in_data_part) ps->update_destination = UD_KEY;
	if (ps->object_depth == 1 && ps->array_depth == 2 && ps->in_data_part) ps->update_destination = UD_KEY;
}

static void parse_byte(struct parser_state *ps, char byte)
{
	if (ps->error != E_NONE) return;

	if (ps->string_state == SS_INSIDE) {
		if (byte == '\\') {
			ps->string_state = SS_ESCAPE;
			return;
		}
		if (byte == '"') {
			ps->string_state = SS_OUTSIDE;
			string_ended(ps);
			return;
		}
	}
	if (ps->string_state != SS_OUTSIDE) {
		ps->string_state = SS_INSIDE;
		if (ps->update_destination == UD_KEY) key_state_update(byte, &ps->key_state);
		else if (ps->update_destination == UD_URL) update_url_state(&ps->url_state, byte);
		return;
	}

	if (byte == ',' || byte == '}' || byte == ']') {
		if (ps->update_destination == UD_VALUE) {
			if (ps->in_data_part) {
				ps->v = finalize_value_state(&ps->value_state);
			}
			ps->update_destination = UD_DISCARD;
		}
	}

	switch(byte) {
	case '"':
		ps->string_state = SS_INSIDE;

		if (ps->update_destination == UD_KEY) {
			if (ps->in_data_part == false && ps->object_depth == 1 && ps->array_depth == 1) {
				key_state_init(&ps->key_state, base_keys);
			} else if (ps->in_data_part == true && ps->object_depth == 1 && ps->array_depth == 2) {
				key_state_init(&ps->key_state, data_keys);
			} else {
				ps->update_destination = UD_DISCARD;
			}
		} /* other string target initializers can happen when the key is found and the UD is set */
		return;
	case '{':
		if (ps->object_depth == MAXDEPTH) {
			ps->error = E_OVERFLOW;
			return;
		}
		if (ps->update_destination != UD_DISCARD && ps->update_destination != UD_KEY) {
			ps->error = E_TYPEERROR;
		}
		ps->object_depth += 1;
		if (ps->object_depth == 1 && ps->array_depth == 2 && ps->in_data_part) {
			/* reference is always base name */
			memcpy(&ps->n, &ps->bn, sizeof(ps->n));
			ps->v = VALUE_NOTPRESENT_SENTINEL;
		}
		maybe_start_new_key(ps);
		return;
	case '}':
		if (ps->object_depth == 0) {
			ps->error = E_NOTJSON;
			return;
		}
		if (ps->object_depth == 1 && ps->array_depth == 2 && ps->in_data_part) {
			const char *ownpath = get_own_path(&ps->n);
			const char *valuetext = ps->v == VALUE_OVERFLOW_SENTINEL ? " (overflow)" : ps->v == VALUE_NOTPRESENT_SENTINEL ? " (not present)" : "";
			if (ownpath) {
				printf("Update event received on %s: %d%s\n", ownpath, ps->v, valuetext);
			}else {
				printf("Ignoring update on foreign URI with value %d%s: ", ps->v, valuetext);
				print_url(&ps->n);
			}
		};
		ps->object_depth -= 1;
		return;
	case '[':
		if (ps->array_depth == MAXDEPTH) {
			ps->error = E_OVERFLOW;
			return;
		}
		if (ps->update_destination != UD_DISCARD && ps->update_destination != UD_KEY) {
			ps->error = E_TYPEERROR;
		}
		ps->array_depth += 1;
		return;
	case ']':
		if (ps->array_depth == 0) {
			ps->error = E_NOTJSON;
			return;
		}
		ps->array_depth -= 1;
		return;
	case ',':
		maybe_start_new_key(ps);

		/* the flip-flop between base and data */
		if (ps->array_depth == 1 && ps->object_depth == 0) ps->in_data_part = ! ps->in_data_part;
		return;
	case '+':
	case '-':
	case '0'...'9':
	case '.':
	case 'e':
	case 'E':
		if (ps->update_destination == UD_VALUE) {
			if (ps->update_destination == UD_VALUE) {
				update_value_state(&ps->value_state, byte);
			} else if (ps->update_destination != UD_DISCARD) {
				ps->error = E_TYPEERROR;
			}
		} else {
			ps->update_destination = UD_DISCARD;
		}
		return;
	case ':':
	case ' ':
	case '\t':
	case '\n':
	case '\r':
		return;
	case 't': /* true */
	case 'f': /* false */
	case 'n': /* null */
		if (ps->update_destination == UD_URL) {
			/* we must treat null in a base uri as if no base uri
			 * was given. treating all atoms like an empty string
			 * is good enough an approximation. as the url_state
			 * initialization code doesn't actually harm the url,
			 * and the base url is set to the hostname as soon as a
			 * "bn" shows up, there's nothing left to do here. */

			/* fall through, we don't need any more bytes */
		}
		ps->update_destination = UD_DISCARD;
		return;
	case 'a': /* remainign cases from true/false/null -- just ignoring them */
	case 'l':
	case 's':
	case 'r':
	case 'u':
		return;
	default:
		ps->error = E_NOTJSON;
		return;
	}
}

void parse(struct parser_state *ps, char *data, size_t data_length)
{
	for (int i = 0; i < data_length; ++i)
	{
		parse_byte(ps, data[i]);
	}
}

int main(int argc, const char *argv[])
{
	if (argc <= 1) {
		printf("Usage: %s filename\n", argv[0]);
		return 1;
	}
	FILE *infile = fopen(argv[1], "r");
	struct parser_state state = {0};
	initialize_own_url(&state.bn);
	while(1)
	{
		char data[256];
		size_t data_length = fread(data, 1, sizeof(data), infile);
		parse(&state, data, data_length);
		if (state.error != E_NONE) {
			if (state.error == E_TYPEERROR)
				printf("Unsurprisingly, the parser failed (type error)\n");
			else
				printf("Surprisingly, the parser failed (%d)\n", state.error);
			return 2;
		}
		if (feof(infile)) return 0;
		if (ferror(infile)) return 1;
	}
}
