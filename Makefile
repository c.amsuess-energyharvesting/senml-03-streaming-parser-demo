CFLAGS += -g3 -Wall -Wpedantic -Wextra

senml-reader:

run: senml-reader
	./senml-reader ./senml-example.json

.PHONY: run
