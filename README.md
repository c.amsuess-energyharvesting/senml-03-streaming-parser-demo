This demo shows how SenML as described in per [draft-jennings-core-senml-03][]
can be parsed in a serial fashion by a device that has insufficient memory to
store two consecutive packages. It requires

* streaming access to the incoming SenML data,
* the protocol, host name and all local URL paths to be present and sorted
  (possibly in flash),
* a struct of 24 bytes for each SenML stream to be read (minimum size
  obtainable by good packing on a 32bit architecture estimated by summing up the
  sizes; real size 56 bytes on amd64),
* ca 2.5k text size for parser (on amd64 with -Os and output statements
  removed, but there's probably ample room for optimization),
* but no RAM string buffers whatsoever,

and provides

* parsing of all valid SenML-03 documents,
* full URLs in the combination of document URL, bn and n, including resolution
  of `./`, `../`, and ignoring (but not being confused by) nonexistant paths on
  the own device,
* parsing all legal JSON numerics into int32 (unless they don't fit or use
  abhorrent exponents),
* recognition of "bn", "n" and "v", and
* tolerance against arbitrary extensions as long as they are valid JSON and not
  exceedingly nested.

The parser accepts quite a number of inputs that are *not* JSON and will report
only the fewest thereof as erroneous, but will abort parsing when a completely
unexpected entity shows up (eg. a number as base name), and will otherwise not
enter a state that could not be entered with a well-formed JSON input as well.

The implementation is rather a quick hack than an optimized or complete JSON
parser, but demonstrates what is possible with -03 as opposed to -01. It is
likely to contain bugs and not fully provide what is announced above, but good
enough to show it's possible.

To adapt this to -04, an additional string buffer would be required to store a
"n" URL that comes before a "bn" on the same record.

[draft-jennings-core-senml-03]: https://datatracker.ietf.org/doc/draft-jennings-core-senml/03/
